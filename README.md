# Welcome to hell

Hell is simple, decentralized, project dependency manager with [semver](http://semver.org/), [cmake](https://cmake.org/) and [git](https://git-scm.com/) integration.

In opposition to other package or dependency managers designed to work on ready to use binaries or scripts
hell was designed to be tool for compiled languages like C or C++ (but can be used for script languages also).

In case of compiled languages it's very important to have correctly compiled libraries and binaries,
which fits best our requirements - for example it matters if package was builded with Release or Debug target
or with some optional features or without them.
Sometimes important is compiler or ABI version and incorrect one may lead us to very hard to find issues.
Things are even more complicated if our project have big dependency tree where some branches may have
conflicting requirements with other.
For more in many cases "System wide" packet installation is not good solution because if we are working on different
projects at one time these projects may conflict on common dependency version or enabled in it features.

From other side things like git submodules many times not help in case when single module is required by many submodules of our project, because of similar reasons. Things can be even more complicated if our project require cross-compilation.

So we need tool which allow us to:
 * resolve our project dependency tree and inform in case of conflicts - it need to care about
   * dependency version conflicts
   * dependency required features conflicts
 * download sources of dependencies when resolved
 * build and install dependencies in correct order

**Hell can do all of these tasks for you!**

Additionally hell try to do so in very convenient and easy way:
 * Hell **use semantic versioning** - using semver range expressions you can describe acceptable for your project dependencies versions. This is small thing but give huge advantage over git submodules.
 * Hell **use heavily git**:
   * use git tags to detect software versions available to use
   * use project repository to store all data required to resolve build and install dependencies **there is no centralized database**
 * Hell **use cmake to build and install** software - today cmake is some kind of standard and every well managed project should support it.
 * Hell is written in nodejs is multi-platform and **works exactly the same on linux, mac os x, windows** and many other operating systems.
 * **Hell is pretty fast** - resolving dependency tree of 100 nodes with 100 versions and 100 configuration parameters each is done in real time even on raspberry pi.
 * Hell configuration file is **easy to read and understand** - no special magic.
 * Hell have great, pretty and colorful, constantly improved command line interface.

Because of these unique features hell will save you a lot of time, nerves and improve daily work organization.

If you need more details please visit our [website](https://rilis.io/projects/hell/).

## [Issue Tracker](https://gitlab.com/rilis/rilis/issues)