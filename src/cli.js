"use strict";

var RepoDb = require("./RepoDb.js");
var Resolver = require("./Resolver.js");
var fs = require("fs");
var path = require("path");
var chalk = require("chalk");
var log = require("./logger.js");
var installSolution = require("./installSolution.js");
var rmRecursiveSync = require("./helpers.js").rmRecursiveSync;
var topoSort = require("./algorithms.js").topoSort;
var validateConfig = require("./validateConfig.js");
var ensureDirSync = require("./helpers.js").ensureDirSync;
var publishUsedRepos = require("./publishAndSearch.js").publish;
var searchProject = require("./publishAndSearch.js").search;

var defaultInjects = ["CMAKE_CXX_STANDARD=11", "CMAKE_C_STANDARD=99", "CMAKE_BUILD_TYPE=Release", "CMAKE_INSTALL_PREFIX=" + path.resolve(path.join(process.cwd(), ".hell", "install"))];

function getHints() {
    return JSON.parse(JSON.stringify({
        string: ["required", "inject", "cmakeGenerator", "search", "workspace", "exportDependencies"],
        boolean: ["publish", "incantations", "install", "plantUml", "color", "commandLog", "debugLog", "infoLog", "buildLog", "warningLog", "errorLog", "stacktraceLog", "verbose", "version", "help"],
        alias: {
            help: ["h", "?"],
            workspace: ["W"],
            version: ["v"],
            required: ["r"],
            inject: ["i"],
            cmakeGenerator: ["G"],
            exportDependencies: ["ed"]
        },
        default: {
            workspace: ".hell",
            search: "",
            publish: true,
            incantations: false,
            verbose: false,
            cmakeGenerator: "Unix Makefiles",
            required: [],
            inject: defaultInjects,
            color: true,
            help: false,
            version: false,
            plantUml: false,
            install: false,
            commandLog: false,
            debugLog: false,
            infoLog: true,
            buildLog: true,
            warningLog: true,
            errorLog: true,
            stacktraceLog: true,
            exportDependencies: null
        },
        description: {
            workspace: "can be used to define custom hell workspace directory",
            search: "can be used to find projects which can be used with hell and mach to your needs",
            publish: "If enabled will report used repositories to hell search server",
            incantations: "If enabled will show used hell incantations for current project and current user",
            verbose: "If enabled will print verbose logs about found configuration, problems, progress etc.",
            inject: "allow to provide flags which should used with cmake as default (can be overwritten by resolved targets)",
            required: "should be used to define required targts, which you want to resolve",
            cmakeGenerator: "specify build system generator which should be used by cmake." + chalk.yellow.bold("\n      Remember to always use the same generator if you use multiple times hell in single project without cleanup. Otherwise cmake may not be able to correctly handle it."),
            color: "when enabled, then will use colorful output",
            help: "show hell help informations",
            version: "show version of hell",
            plantUml: "if enabled will generate dependency graph, which can be visualized with plantUml",
            install: "if enabled then will build and install targets with cmake",
            commandLog: "if enabled will print details about executed commands",
            debugLog: "if enabled will print debug logs",
            infoLog: "if enabled will print information logs",
            buildLog: "if enabled will print progress of build from cmake",
            warningLog: "if enabled will print warnings",
            errorLog: "if enabled will print errors",
            stacktraceLog: "if enabled may include stacktraces in some situations to logs",
            exportDependencies: "if used will store resolved dependences with used versions in specified file in JSON format"
        }
    }));
}

function getLoggerConfig() {
    return {
        "verbose": {
            style: chalk.white
        },
        "command": {
            style: chalk.green
        },
        "debug": {
            style: chalk.bold.green
        },
        "info": {
            style: chalk.white
        },
        "build": {
            style: chalk.grey
        },
        "warning": {
            output: console.warn.bind(console),
            style: chalk.bold.yellow
        },
        "error": {
            output: console.error.bind(console),
            style: chalk.bold.red
        },
        "stacktrace": {
            output: console.error.bind(console),
            style: chalk.red
        },
        "vip": {
            style: chalk.magenta
        }
    };
}

function parseOptions(args, hints) {
    hints.default.inject = [];
    hints.unknown = function(arg) {
        unexpected.push(arg);
        return false;
    };

    var unexpected = [];
    var parsed = require('minimist')(args, hints);
    unexpected = parsed._.concat(unexpected);

    function reduceInjects(injects) {
        var injectsMap = injects.reduce(function(acc, val) {
            var key = val.substr(0, val.indexOf("="));
            var value = val.substr(val.indexOf("=") + 1);
            acc[key] = value;
            return acc;
        }, {});

        return Object.keys(injectsMap).map(function(key) {
            return key + "=" + injectsMap[key];
        });
    }

    function validateOptions(unexpected, options) {
        var errors = [];
        if (unexpected.length > 0) {
            errors.push(new Error("Unexpected options: " + JSON.stringify(unexpected)));
        }

        hints.boolean.forEach(function(boolOption) {
            if (options[boolOption] !== true && options[boolOption] !== false) {
                errors.push(new Error("Option `" + boolOption + "` must be true or false"));
            }
        });

        var newRequired = [].concat(options.required);
        options.required = newRequired.filter(function(item, pos) {
            return newRequired.indexOf(item) === pos;
        });

        var workspaceDirectory;
        if (path.isAbsolute(options.workspace)) {
            workspaceDirectory = path.resolve(options.workspace);
        } else {
            workspaceDirectory = path.resolve(path.join(process.cwd(), options.workspace));
        }
        options.workspace = workspaceDirectory;
        options.inject = reduceInjects(defaultInjects.concat(["CMAKE_INSTALL_PREFIX=" + path.resolve(path.join(workspaceDirectory, "install"))], options.inject));

        return {
            errors: errors,
            options: options
        };
    }

    return validateOptions(unexpected, hints.string.concat(hints.boolean).reduce(function(result, current) {
        result[current] = parsed[current];
        return result;
    }, {}));
}

function configureLogger(options) {
    var loggerConfig = getLoggerConfig();
    Object.keys(loggerConfig).forEach(function(loggerName) {
        if (loggerName === "vip") {
            loggerConfig[loggerName].visible = true;
        } else if (loggerName === "verbose") {
            loggerConfig[loggerName].visible = options[loggerName];
        } else {
            loggerConfig[loggerName].visible = options[loggerName + "Log"];
        }
        log.instance.add(loggerName, loggerConfig[loggerName]);
    });
    log.instance.add("default", {
        visible: true
    });
}

function showVersion() {
    log("vip")(chalk.reset.bold("hell, version 1.3.0\n"));
}

function helpMe(errors, hints) {
    errors.forEach(function(error) {
        log("error")("%s", error.message);
        log("stacktrace")("%s", error.stack);
    });
    showVersion();

    var optInfo = hints.string.concat(hints.boolean);
    optInfo = optInfo.reduce(function(knownOpts, longOpt) {
        knownOpts[longOpt] = {
            alias: hints.alias[longOpt] || [],
            default: hints.default[longOpt],
            description: hints.description[longOpt]
        };
        return knownOpts;
    }, {});

    function optionWithAlias(opt, optInfo, sep) {
        function withDash(opt) {
            if (opt.length === 1) {
                return "-" + opt;
            } else {
                return "--" + opt;
            }
        }

        function aliasReducer(curr, o) {
            return curr + sep + withDash(o);
        }

        return withDash(opt) + optInfo[opt].alias.reduce(aliasReducer, "");
    }

    log("vip")(chalk.reset("usage: %s %s"),
        path.parse(process.argv[1]).name,
        Object.keys(optInfo).reduce(function(curr, longOpt) {
            return curr + "[" + optionWithAlias(longOpt, optInfo, "|") + "] ";
        }, ""));

    log("vip")(chalk.blue.underline.bold("\nOptions (default value in round brackets):"));
    for (var opt in optInfo) {
        log("vip")(chalk.bold("  %s (%j)") + " - %s",
            optionWithAlias(opt, optInfo, ", "),
            optInfo[opt].default,
            chalk.reset(optInfo[opt].description));
    }
    log("vip")(chalk.cyan("\n note: all boolean options can be used with 'no' prefix to disable option eg." +
        " --color enable 'color' option, while --no-color disable it."));
}

function readConfig(cwd) {
    return JSON.parse(fs.readFileSync(path.resolve(path.join(cwd, ".hell.json")), "utf-8"));
}

function generatePlantUml(solution) {
    function getName(source) {
        if (source !== "root") {
            return chalk.red("[") + chalk.green(source) +
                chalk.magenta("@") +
                chalk.green(solution[source].version) + chalk.red("]");
        } else {
            return chalk.red("[") + chalk.green(source) + chalk.red("]");
        }
    }

    log("info")(chalk.bold.cyan("  @startuml"));

    Object.keys(solution).forEach(function(required) {
        log("info")("    %s\n      %s %s",
            getName(required),
            chalk.magenta("note right:"),
            chalk.cyan(JSON.stringify(solution[required].targets)));
    });
    log("info")("");

    Object.keys(solution).forEach(function(required) {
        solution[required].requiredBy.forEach(function(requireing) {
            log("info")("    %s", getName(requireing) + chalk.bold.cyan(" --> ") + getName(required));
        });
    });
    log("info")(chalk.bold.cyan("  @enduml"));
}

function promiseCreateRepoDb(workspaceDir) {
    return new Promise(function(resolve) {
        resolve(new RepoDb(workspaceDir));
    });
}

function promiseResolveSolution(resolver, rootConfig, options) {
    return new Promise(function(resolve) {
        resolve(resolver.resolve({
            config: rootConfig,
            required: options.required
        }));
    });
}

function promiseExecuteTasks(options, cwd) {
    var workspaceDir = options.workspace;

    return promiseCreateRepoDb(workspaceDir).then(function(repoDb) {

        var resolver = new Resolver(function(repoUrl) {
            return repoDb.getInfo(repoUrl);
        }, options);

        var rootConfig;
        try {
            rootConfig = readConfig(cwd);
            repoDb.setRootConfig(rootConfig);
        } catch (e) {
            log("error")("Can't read hell config for your project!");
            throw e;
        }

        log("debug")("root config: %j", rootConfig);

        log("info")(chalk.blue.underline.bold("Resolving dependencies"));

        return promiseResolveSolution(resolver, rootConfig, options).then(function(solution) {
            if (solution !== null) {
                log("debug")("Final solution: %j", solution);
                return solution;
            } else {
                throw new Error("Cannot find acceptable dependencies :/");
            }
        }).then(function(solution) {
            var solutionInstallOrder = topoSort(solution).reverse();
            log("info")(chalk.blue.underline.bold("Detected solution:"));
            solutionInstallOrder.forEach(function(repoUrl) {
                if (repoUrl !== "root") {
                    log("info")("  %s " + chalk.magenta("@") + " %s " + chalk.magenta("-> ") + chalk.cyan("%j"),
                        chalk.green(repoUrl),
                        chalk.green(solution[repoUrl].version),
                        solution[repoUrl].targets);
                }
            });

            if (options.plantUml) {
                log("info")("%s", chalk.blue.underline.bold("Dependency graph:"));
                generatePlantUml(solution);
            }

            if (options.exportDependencies !== null) {
                var dependenciesList = solutionInstallOrder.filter(function(e) {
                    return e !== "root";
                }).map(function(e) {
                    return {
                        url: e,
                        version: solution[e].version
                    };
                });
                fs.writeFileSync(options.exportDependencies, JSON.stringify(dependenciesList));
            }

            if (options.install) {
                return installSolution(solution, solutionInstallOrder, workspaceDir, options).then(function() {
                    if (options.publish) {
                        return publishUsedRepos(solutionInstallOrder).then(function() {}, function() {});
                    }
                });
            }
        });
    });
}

function getIncantations(workspaceDirectory) {
    var incantations = [];

    try {
        incantations = JSON.parse(fs.readFileSync(path.resolve(path.join(workspaceDirectory, "incantations.json")), "utf-8"));
    } catch (e) {}
    return incantations;
}

function showIncantations(workspaceDirectory) {
    var incantations = getIncantations(workspaceDirectory);

    incantations.forEach(function(incantation) {
        log("info")("  * " + chalk.yellow("%s"), incantation.map(function(arg) {
            return "\"" + arg + "\"";
        }).join(" "));
    });
}

function run(args, cwd) {

    if (args.length > 0 && args[0] === "--search") {
        var request = args.slice(1).join(" ");
        chalk.enabled = true;
        log.instance.add("default", {
            visible: true
        });
        return searchProject(request).then(function(response) {
            log("default")(chalk.white.bold("%d projects found for: ") + chalk.white("%s\n"), response.length, request);
            response.forEach(function(foundProject) {
                log("default")(chalk.green.green("%s") + chalk.magenta(" -> ") + chalk.green("%s"), foundProject.name, foundProject.url);
                log("default")(chalk.cyan("  Popularity: ") + chalk.magenta(" %d"), foundProject.popularity);
                log("default")(chalk.cyan("  Topics: ") + chalk.yellow("    %s"), foundProject.topics.join(", "));
                log("default")(chalk.cyan("  Description: ") + chalk.bold.blue("%s\n"), foundProject.description);
            });
            return 3;
        }).catch(function(e) {
            log("error")(e);
            return -3;
        });
    } else {
        if (args.length === 0) {
            var defaultWorkspace = path.resolve(path.join(cwd, ".hell"));
            var incantations = getIncantations(defaultWorkspace);
            if (incantations.length !== 0) {
                args = incantations[0];
            }
        }

        var options = parseOptions(args, getHints());
        chalk.enabled = options.options.color;
        configureLogger(options.options);
        log("debug")("Used options: %j", options.options);
        if (options.errors.length !== 0 || options.options.help) {
            helpMe(options.errors, getHints());
            return Promise.resolve(-1);
        }
        options = options.options;

        if (options.version) {
            showVersion();
            saveIncantation(args, options.workspace);
            return Promise.resolve(1);
        }

        if (options.incantations) {
            showIncantations(options.workspace);
            return Promise.resolve(2);
        }

        if (validateConfig(cwd) !== true) {
            return Promise.resolve(-1);
        }

        return promiseExecuteTasks(options, cwd).then(function() {
            saveIncantation(args, options.workspace);
            return 0;
        }).catch(function(e) {
            log("error")("%s", e.message);
            log("stacktrace")("%s", e.stack);
            return -2;
        });
    }
}

function saveIncantation(args, workspaceDirectory) {
    var incantatoinsPath = path.resolve(path.join(workspaceDirectory, "incantations.json"));
    var incantations = [];

    try {
        incantations = JSON.parse(fs.readFileSync(incantatoinsPath, "utf-8"));
    } catch (e) {}

    incantations.unshift(args);

    try {
        fs.writeFileSync(incantatoinsPath, JSON.stringify(incantations));
    } catch (e) {}
}

function cli(args, cwd) {
    return run(args, cwd).then(function(v) {
        process.exit(v > 0 ? 0 : v);
    });
}

module.exports = cli;