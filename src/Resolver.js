'use strict';
var semver = require("semver");
var ProposalGenerator = require("./ProposalGenerator.js");
var algorithms = require("./algorithms.js");
var log = require("./logger.js");

function mergeConfigs(configs) {
    var merged = {};

    function update(destination, dependency, requiredBy) {
        if (destination[dependency.source]) {
            destination[dependency.source] = {
                version: algorithms.getUniques(destination[dependency.source].version.concat(dependency.version.split(" "))),
                required: algorithms.getUniques(destination[dependency.source].required.concat(dependency.required)),
                targets: algorithms.getUniques(destination[dependency.source].targets.concat(dependency.required)),
                requiredBy: algorithms.getUniques(destination[dependency.source].requiredBy.concat(requiredBy))
            };
        } else {
            destination[dependency.source] = {
                version: dependency.version.split(" "),
                required: dependency.required,
                targets: dependency.required,
                requiredBy: [].concat(requiredBy)
            };
        }
    }
    configs.forEach(function(config) {
        config.required.forEach(function(dependency) {
            if (config.config[dependency]) {
                config.config[dependency].forEach(function(subDependency) {
                    update(merged, subDependency, config.requiredBy);
                });
            }
        });
    });
    return merged;
}


function fetchExactVersions(getRepoInfo, config, baseSolution) {
    var exactConfig = {};
    Object.keys(config).forEach(function(repoUrl) {
        try {
            var repoInfo = getRepoInfo(repoUrl);
            var acceptedRange = config[repoUrl].version.join(" ");
            var acceptedVersions = repoInfo.map(function(version) {
                return version.version;
            }).filter(function(version) {
                if (repoUrl in baseSolution) {
                    return baseSolution[repoUrl].version === version;
                } else {
                    return true;
                }
            }).filter(function(version) {
                if (semver.valid(version)) {
                    return semver.satisfies(version, acceptedRange);
                } else {
                    return config[repoUrl].version.every(function(e) {
                        return e === version || e === "*";
                    });
                }
            });

            exactConfig[repoUrl] = {
                version: acceptedVersions,
                required: config[repoUrl].required,
                targets: config[repoUrl].targets,
                requiredBy: config[repoUrl].requiredBy
            };
        } catch (e) {
            log("error")("%s", e.message);
            log("stacktrace")("%s", e.stack);
            exactConfig[repoUrl] = {
                version: [],
                required: config[repoUrl].required,
                targets: config[repoUrl].targets,
                requiredBy: config[repoUrl].requiredBy
            };
        }
    });

    return exactConfig;
}

function Resolver(getRepoInfo, options) {
    this._options = options;

    this._getRepoInfo = getRepoInfo;
}

Resolver.prototype._resolve = function(currentConfigs, baseSolution) {
    var getRepoInfo = this._getRepoInfo;
    var options = this._options;

    var mergedConfigs = fetchExactVersions(getRepoInfo, mergeConfigs(currentConfigs), baseSolution);
    log("debug")("mergedConfigs: %j, baseSolution: %j", mergedConfigs, baseSolution);
    if (Object.keys(mergedConfigs).length === 0) {
        return baseSolution;
    } else {
        var proposalGenerator = new ProposalGenerator(getRepoInfo, mergedConfigs, baseSolution);
        for (var proposal = proposalGenerator.next(); proposal !== null; proposal = proposalGenerator.next()) {
            var solution = this._resolve(proposal.configs, proposal.partial);
            if (solution !== null) {
                return solution;
            }
        }
        return null;
    }
};

Resolver.prototype.resolve = function(currentConfig) {
    currentConfig.requiredBy = ["root"];
    return this._resolve([currentConfig], {
        "root": {
            requiredBy: [],
            targets: currentConfig.required
        }
    });
};

Resolver._mergeConfigs = mergeConfigs;

module.exports = Resolver;