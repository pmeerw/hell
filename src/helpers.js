'use strict';
var child_process = require("child_process");
var fs = require("fs");
var path = require("path");
var log = require("./logger.js");

function executeCommand(programName, programWorkingDirectory, args) {
    log("command")("Executing in %s command: %s with args: %j", programWorkingDirectory, programName, args);
    var result = child_process.spawnSync(programName, args, {
        cwd: programWorkingDirectory,
        encoding: "utf-8"
    });

    if (result.error) {
        log("command")("Execution failure: %s", result.error);
        throw result.error;
    } else if (result.status !== 0) {
        log("command")("Execution failure(status: %d): %s", result.status, result.stderr);
        throw new Error("Incorrect status: " + result.status + " for: " + JSON.stringify({
            programName: programName,
            args: args,
            cwd: programWorkingDirectory
        }) + " stderr: " + result.stderr);
    } else {
        log("command")("Execution success: %s", result.stdout);
        return result.stdout;
    }
}

function ensureDirSync(targetDir) {
    targetDir.split(path.sep).forEach(function(dir, index, splits) {
        var dirPath = splits.slice(0, index + 1).join(path.sep);
        if (index > 0) {
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath);
            }
        }
    });
}

function existDirSync(dirPath) {
    return fs.existsSync(dirPath);
}

function rmRecursiveSync(dirPath) {
    var files;
    try {
        files = fs.readdirSync(dirPath);
    } catch (e) {
        return;
    }
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            var filePath = path.join(dirPath, files[i]);
            if (fs.statSync(filePath).isFile()) {
                fs.unlinkSync(filePath);
            } else {
                rmRecursiveSync(filePath);
            }
        }
    }
    fs.rmdirSync(dirPath);
}

module.exports = {
    executeCommand: executeCommand,
    ensureDirSync: ensureDirSync,
    rmRecursiveSync: rmRecursiveSync,
    existDirSync: existDirSync
};