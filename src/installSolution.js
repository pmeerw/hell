"use strict";
var log = require("./logger.js");
var path = require("path");
var createRepoHash = require("./algorithms.js").createRepoHash;
var chalk = require("chalk");
var spawn = require("child_process").spawn;
var ensureDirSync = require("./helpers.js").ensureDirSync;
var existDirSync = require("./helpers.js").existDirSync;
var git = require("./git.js");
var updateDirectory = require("./updateDirectory.js");

function ensureDir(dir) {
    return new Promise(function(resolve) {
        ensureDirSync(dir);
        resolve();
    });
}

function cmake(buildDir, args) {
    return new Promise(function(resolve, reject) {
        var child = spawn("cmake", args, {
            cwd: buildDir,
            stdio: [process.stdin, "pipe", "pipe"]
        });

        var stdoutBuffer = "";
        var stderrBuffer = "";
        child.stdout.on('data', function(data) {
            stdoutBuffer += data;
            var lines = stdoutBuffer.split('\n');
            stdoutBuffer = lines[lines.length - 1];
            lines.pop();
            lines.forEach(function(line) {
                log("build")("      %s", line);
            });
        });

        child.stderr.on('data', function(data) {
            stderrBuffer += data;
            var lines = stderrBuffer.split('\n');
            stderrBuffer = lines[lines.length - 1];
            lines.pop();
            lines.forEach(function(line) {
                log("error")("      %s", line);
            });
        });

        child.on('close', function(status) {
            if (status === 0) {
                resolve();
            } else {
                reject(new Error("Unexpected cmake exit status: " + status));
            }
        });

        child.on('error', function(error) {
            reject(error);
        });
    });
}

function targetsToCmakeOptions(targets) {
    return targets.filter(function(target) {
        return target !== "=";
    }).map(function(target) {
        return "-D" + target;
    });
}

function createCounter(partNumber, numberOfParts) {
    return chalk.bold(chalk.magenta("[") + chalk.green(partNumber) +
        chalk.magenta("/") +
        chalk.green(numberOfParts) + chalk.magenta("]"));
}

function updatePrefixPath(cmakeOptions, installPath) {
    var havePrefixPath = false;
    cmakeOptions = cmakeOptions.map(function(option) {
        if (option.substr(0, 20) === "-DCMAKE_PREFIX_PATH=") {
            havePrefixPath = true;
            return option + ";" + installPath;
        } else {
            return option;
        }
    });
    if (havePrefixPath === false) {
        cmakeOptions.push("-DCMAKE_PREFIX_PATH=" + installPath);
    }
    return cmakeOptions;
}

function installStepByStep(params, partNumber, numberOfParts) {
    if (params.toInstall.length === 0) {
        return;
    } else {
        var repoUrl = params.toInstall[0];
        params.toInstall.shift();
        log("info")("  %s %s", chalk.bold.blue(createCounter(partNumber, numberOfParts)), chalk.green(repoUrl));
        var hash = createRepoHash(repoUrl);
        var repoDbPath = path.join(params.workspace, "repodb", hash);
        var repoPath = path.join(params.workspace, "source", hash);
        var buildDir = path.join(params.workspace, "build", hash);
        var currentRepoHash = "";

        return (new Promise(function(resolve) {
            var repo = git(repoDbPath);
            currentRepoHash = repo.getVersionHash(params.solution[repoUrl].version);
            repo.checkout("--quiet", "--force", currentRepoHash);
            updateDirectory(repoDbPath, repoPath);
            resolve();
        })).then(function() {
            return ensureDir(buildDir);
        }).then(function() {
            var cmakeOptions = updatePrefixPath(params.injectOptions
                    .concat(targetsToCmakeOptions(params.solution[repoUrl].targets)), params.installPath)
                .concat("-DHELL_USED=ON",
                    "-DHELL_CURRENT_PROJECT_URI=" + repoUrl,
                    "-DHELL_CURRENT_PROJECT_HASH=" + currentRepoHash,
                    "-DHELL_CURRENT_PROJECT_VERSION=" + params.solution[repoUrl].version);

            log("info")("    Build based on: %s", chalk.magenta(JSON.stringify(cmakeOptions, null, 0)));
            return cmake(buildDir, [repoPath, "--no-warn-unused-cli", "-G", params.options.cmakeGenerator].concat(cmakeOptions));
        }).then(function() {
            return cmake(buildDir, ["--build", buildDir, "--config", params.buildType, "--target", "install"]);
        }).then(function() {
            return installStepByStep(params, partNumber + 1, numberOfParts);
        });
    }
}

function installSoultion(solution, solutionInstallOrder, workspace, options) {
    var injectOptions = targetsToCmakeOptions(options.inject);
    var optInstallPath = injectOptions.find(function(element) {
        return element.substr(0, 23) === "-DCMAKE_INSTALL_PREFIX=";
    });
    var installPath = path.resolve(optInstallPath.substr(23));
    injectOptions = injectOptions.filter(function(p) {
        return p.substr(0, 23) !== "-DCMAKE_INSTALL_PREFIX=";
    });
    injectOptions.push("-DCMAKE_INSTALL_PREFIX=" + installPath);

    var buildType = injectOptions.find(function(e) {
        return e.substr(0, 19) === "-DCMAKE_BUILD_TYPE=";
    }).substr(19);

    log("info")(chalk.bold.underline.blue("Installing solution"));
    return ensureDir(installPath)
        .then(function() {
            return JSON.parse(JSON.stringify(solutionInstallOrder)).filter(function(v) {
                if (v === "root") {
                    return false;
                } else {
                    return true;
                }
            });
        })
        .then(function(toInstall) {
            return installStepByStep({
                toInstall: toInstall,
                solution: solution,
                injectOptions: injectOptions,
                installPath: installPath,
                workspace: workspace,
                buildType: buildType,
                options: options
            }, 1, toInstall.length);
        })
        .then(function() {
            var rootOptions = updatePrefixPath(injectOptions
                .concat(targetsToCmakeOptions(solution.root.targets)), installPath);
            log("info")("%s", chalk.bold.underline.blue("To build your project use these cmake parameters:"));
            rootOptions.forEach(function(option) {
                log("info")("  %s", chalk.magenta(option));
            });
        });
}
module.exports = installSoultion;