"use strict";
var semver = require("semver");
var chalk = require("chalk");
var log = require("./logger.js");
var fs = require("fs");
var path = require("path");

function readConfig(cwd) {
    return JSON.parse(fs.readFileSync(path.resolve(path.join(cwd, ".hell.json")), "utf-8"));
}

module.exports = function(cwd) {
    var config;
    try {
        config = readConfig(cwd);
    } catch (e) {
        log("error")(chalk.reset.red.bold("Unfortunately there is problem to load your Hell config - there can be some reasons:"));
        log("error")(chalk.reset.red("   * .hell.json not exist"));
        log("error")(chalk.reset.red("   * .hell.json is not in json format"));
        log("error")(chalk.reset.red("   * your user account do not have enaught privilages to read .hell.json"));
        log("error")(chalk.reset.red.bold("\nProbably this will be more or less helpfull for you to find reason:"));
        log("error")(chalk.reset.red(e.stack));
        return false;
    }

    var invalidTargets = Object.keys(config).filter(function(target) {
        return target.indexOf("=") < 0;
    });

    if (invalidTargets.length > 0) {
        log("error")(chalk.reset.red.bold("Following entries are not valid hell targets: ") +
            chalk.reset.red("%j") +
            chalk.reset.bold.red(". Please fix it."), invalidTargets);
        log("error")(chalk.reset.green.bold("Valid target synthax is: 'TARGET_KEY=TARGET_VALUE'"));
        return false;
    }

    var targets = Object.keys(config).reduce(function(acc, val) {
        var key = val.substr(0, val.indexOf("="));
        var value = val.substr(val.indexOf("=") + 1);
        if (key in acc) {
            acc[key].push(value);
        } else {
            acc[key] = [value];
        }
        return acc;
    }, {});

    if (!("" in targets)) {
        log("error")(chalk.reset.red.bold("Default target was not found in your hell config"));
        log("error")(chalk.reset.red("Please add it to your .hell.json - it's required for all projects even if it's empty!"));
        return false;
    }

    var validTargets = Object.keys(config);
    for (var i = 0; i < validTargets.length; ++i) {
        var dependencies = config[validTargets[i]];
        if (!Array.isArray(dependencies)) {
            log("error")("Target " + chalk.magenta("%s") + " dependencies must be specified as Array, please fix it!", validTargets[i]);
            return false;
        }
        for (var j = 0; j < dependencies.length; ++j) {
            var dependency = dependencies[j];
            if (!("source" in dependency)) {
                log("error")("Target " + chalk.magenta("%s") + " dependency " + chalk.magenta("%d") + " do not have `source` entry, which is required and must be valid git repo URL string!", validTargets[i], j);
            }
            if (!("version" in dependency)) {
                log("error")("Target " + chalk.magenta("%s") + " dependency " + chalk.magenta("%d") + " do not have `version` entry, which is required and must be valid semantiv versioning string!", validTargets[i], j);
            }
            if (!("required" in dependency)) {
                log("error")("Target " + chalk.magenta("%s") + " dependency " + chalk.magenta("%d") + " do not have `required` entry, which must be specified as Array (even empyt) of strings representing needed targets from dependency!", validTargets[i], j);
            }
            if (semver.validRange(dependency.version) === null) {
                log("verbose")(chalk.bold.red("Target ") + chalk.magenta("%s") + chalk.bold.red(" dependency ") + chalk.magenta("%d") + chalk.bold.red(" `version` entry is not valid Semantic Versioning range - possibly you are trying to not use Semantic Versioning.\nIt will work with hell, however we strongly recommend to not follow this way in serious projects."), validTargets[i], j);
            }
            var invalidRequired = dependency.required.filter(function(req) {
                return (typeof req !== 'string') || (req.indexOf("=") < 0);
            });
            if (invalidRequired.length > 0) {
                log("error")("Target " + chalk.magenta("%s") + " dependency " + chalk.magenta("%d") + " `required` entry contain invalid required targets: " + chalk.magenta("%j"), validTargets[i], j, invalidRequired);
                return false;
            }
        }
    }
    log("verbose")(chalk.bold.green.underline("Current project visible targets and acceptable options:"));
    log("verbose")("- " + chalk.reset.cyan("Default(`=`)"));
    Object.keys(targets).forEach(function(target) {
        if (target !== "") {
            log("verbose")("- " + chalk.reset.cyan(target) + " = " + chalk.reset.yellow("%j"), targets[target]);
        }
    });

    log("verbose")(chalk.green.bold.underline("Current project targets description:"));
    for (var k = 0; k < validTargets.length; ++k) {
        var deps = config[validTargets[k]];
        if (deps.length > 0) {
            log("verbose")(chalk.reset.cyan("  * ") + chalk.reset.cyan.bold(validTargets[k] === "=" ? "Default(`=`)" : validTargets[k]) + chalk.reset.cyan(" will require:"));
            for (var l = 0; l < deps.length; ++l) {
                log("verbose")("    - " + chalk.green("%s") + chalk.magenta(" @ ") + "(" + chalk.green("%s") + ")" + chalk.magenta(" -> ") + chalk.cyan("%j"), deps[l].source, deps[l].version, deps[l].required);
            }
        } else {
            log("verbose")(chalk.reset.cyan("  * ") + chalk.reset.cyan.bold(validTargets[k] === "=" ? "`Default(=)`" : validTargets[k]) + chalk.reset.cyan(" will require " + chalk.green("nothing.")));
        }
    }

    return true;
};