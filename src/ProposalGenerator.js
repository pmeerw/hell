"use strict";
var algorithms = require("./algorithms.js");
var log = require("./logger.js");

function configToArray(config) {
    var result = [];
    for (var repoUrl in config) {
        result.push({
            source: repoUrl,
            version: config[repoUrl].version,
            required: config[repoUrl].required,
            targets: config[repoUrl].targets,
            requiredBy: config[repoUrl].requiredBy
        });
    }
    return result;
}

function getNumberOfPermutations(config) {
    var numPerms = 1;
    for (var i = 0; i < config.length; ++i) {
        numPerms *= config[i].version.length;
    }
    return numPerms;
}

function getDivisors(config) {
    var divisors = [];
    for (var i = config.length - 1; i >= 0; --i) {
        divisors[i] = divisors[i + 1] ? divisors[i + 1] * config[i + 1].version.length : 1;
    }
    return divisors;
}

var tagValidationRegex = /\w*=\w*/i;

function haveInvalidTargets(solution, getRepoInfo) {
    function getConfig(source, version) {
        if (source === "root") {
            return getRepoInfo(source).config;
        }
        return getRepoInfo(source).reduce(function(c, v) {
            if (v.version === version) {
                return v.config;
            } else {
                return c;
            }
        }, {});
    }

    function targetsAreConflictiong(targets) {
        return algorithms.getUniques(targets.map(function(target) {
            return target.substring(0, target.indexOf('='));
        })).length !== targets.length;
    }

    for (var source in solution) {
        if (targetsAreConflictiong(solution[source].targets)) {
            // check if targets are not conflicting for given source and version
            log("debug")("haveInvalidTargets: conflicting");
            return true;
        }

        for (var targetNumber in solution[source].targets) {
            if (!solution[source].targets[targetNumber].match(tagValidationRegex)) {
                // check if target is in form A=B
                log("debug")("haveInvalidTargets: not match to tagValidationRegex");
                return true;
            } else if (!(solution[source].targets[targetNumber] in getConfig(source, solution[source].version))) {
                // check if targets exists for all sources in given versions in repo db
                log("debug")("haveInvalidTargets: target not exist (%j %j vs %j)",
                    source,
                    solution[source].targets[targetNumber],
                    getConfig(source, solution[source].version));
                return true;
            }
        }
    }
    return false;
}

function mergePartialSolutions(oldSolution, newSolution, getRepoInfo) {

    var merged = JSON.parse(JSON.stringify(oldSolution));
    for (var repo in newSolution) {
        if (newSolution[repo].source in merged &&
            merged[newSolution[repo].source].version !== newSolution[repo].version) {
            return null;
        }
        if (merged[newSolution[repo].source] === undefined) {
            merged[newSolution[repo].source] = {
                targets: [],
                requiredBy: []
            };
        }
        merged[newSolution[repo].source].version = newSolution[repo].version;
        merged[newSolution[repo].source].targets = algorithms
            .getUniques(merged[newSolution[repo].source].targets.concat(newSolution[repo].targets));
        merged[newSolution[repo].source].requiredBy = algorithms
            .getUniques(merged[newSolution[repo].source].requiredBy.concat(newSolution[repo].requiredBy));
    }
    if (haveInvalidTargets(merged, getRepoInfo)) {
        log("debug")("Invalid targets detected for: %j", merged);
        return null;
    } else if (algorithms.haveCycle(merged)) {
        log("debug")("Cycle detected for: %j", merged);
        return null;
    } else {
        return merged;
    }
}

function ProposalGenerator(getRepoInfo, config, baseSolution) {
    this._getRepoInfo = getRepoInfo;
    this._config = configToArray(config);
    this._baseSolution = baseSolution;
    this._proposalCounter = 0;
    this._numberOfPermutations = getNumberOfPermutations(this._config);
    this._divisors = getDivisors(this._config);
}

function getConfigFromRepoInfoVersion(repoInfo, interestingVersion) {
    return repoInfo.filter(function(v) {
        return v.version === interestingVersion;
    })[0].config;
}

ProposalGenerator.prototype._getPermutation = function() {
    try {
        var config = this._config;
        var divisors = this._divisors;
        var n = this._proposalCounter;
        var getRepoInfo = this._getRepoInfo;

        var configs = [];
        var partial = [];
        for (var isource = 0; isource < config.length; ++isource) {
            var curArray = config[isource].version;
            var choosenVersion = curArray[Math.floor(n / divisors[isource]) % curArray.length];
            configs.push({
                config: getConfigFromRepoInfoVersion(getRepoInfo(config[isource].source), choosenVersion),
                required: config[isource].required,
                requiredBy: config[isource].source
            });
            partial.push({
                source: config[isource].source,
                version: choosenVersion,
                targets: config[isource].targets,
                requiredBy: config[isource].requiredBy
            });
        }
        var mergedPartial = mergePartialSolutions(this._baseSolution, partial, getRepoInfo);
        if (mergedPartial !== null) {
            return {
                configs: configs,
                partial: mergedPartial
            };
        }
    } catch (e) {
        log("error")("%s", e.message);
        log("stack")("%s", e.stack);
    }
    return null;
};

ProposalGenerator.prototype.next = function() {
    while (this._proposalCounter < this._numberOfPermutations) {
        var proposal = this._getPermutation();
        this._proposalCounter++;
        if (proposal !== null) {
            return proposal;
        }
    }
    return null;
};

module.exports = ProposalGenerator;