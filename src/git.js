"use strict";
var executeCommand = require("./helpers.js").executeCommand;
var path = require("path");

function Repo(repoPath) {
    this._workspace = path.resolve(repoPath);
}

["checkout", "clone", "show-ref"].forEach(function(operation) {
    Repo.prototype[operation] = function() {
        return executeCommand("git", this._workspace, [operation].concat(Array.prototype.slice.call(arguments)));
    };
});

var versionExpr = /^[0-9a-f]{40} refs\/(heads\/.*|remotes\/origin\/HEAD)$/;
Repo.prototype.getVersions = function() {

    return this["show-ref"].apply(this, []).split("\n").filter(function(line) {
        return line.match(versionExpr) === null;
    }).map(function(ref) {
        return ref.substr(ref.lastIndexOf("/") + 1);
    }).filter(function(version) {
        return version.length > 0;
    });
};

Repo.prototype.getVersionHash = function(version) {
    return this["show-ref"].apply(this, []).split("\n").filter(function(line) {
        return line.match(versionExpr) === null;
    }).map(function(ref) {
        return {
            version: ref.substr(ref.lastIndexOf("/") + 1),
            hash: ref.substr(0, ref.indexOf(" "))
        };
    }).reduce(function(acc, entry) {
        acc[entry.version] = entry.hash;
        return acc;
    }, {})[version];
};

function git(repoPath) {
    return new Repo(repoPath);
}

module.exports = git;