"use strict";
var util = require("util");

function Logger(options) {
    var output = options.output || console.log.bind(console);
    if (options.visible === false) {
        this._log = function() {};
    } else if (options.style !== undefined) {
        this._log = function(s) {
            output("%s", options.style(s));
        };
    } else {
        this._log = function(s) {
            output("%s", s);
        };
    }

}

Logger.prototype.log = function() {
    this._log(util.format.apply(util, arguments));
};

function Loggers() {
    this._loggers = {
        default: new Logger({
            visible: false
        })
    };
}

Loggers.prototype.log = function(type) {
    var logger = (type in this._loggers) ? this._loggers[type] : this._loggers["default"];
    return logger.log.bind(logger);
};

Loggers.prototype.add = function(name, options) {
    this._loggers[name] = new Logger(options);
};

var globalLoggers = new Loggers();

function globalLog(type) {
    return globalLoggers.log(type);
}

globalLog.instance = globalLoggers;

module.exports = globalLog;