'use strict';
var algorithms = require("../src/algorithms.js");
var expect = require("chai").expect;

describe("algorithms", function() {
    describe("topoSort", function() {
        it("empty graph result empty result", function() {
            expect([]).to.deep.equal(algorithms.topoSort({}));
        });

        it("single node graph result single element list", function() {
            expect(["1"]).to.deep.equal(algorithms.topoSort({
                "1": {
                    requiredBy: []
                }
            }));
        });

        it("3 node graph cycle is detected", function() {

            expect(function() {
                algorithms.topoSort({
                    "1": {
                        requiredBy: ["3"]
                    },
                    "2": {
                        requiredBy: ["1"]
                    },
                    "3": {
                        requiredBy: ["2"]
                    }
                });
            }).to.throw(Error);
        });

        it("4 node diamond can be sorted", function() {
            expect(["1", "2", "3", "4"]).to.deep.equal(algorithms.topoSort({
                "1": {
                    requiredBy: []
                },
                "2": {
                    requiredBy: ["1"]
                },
                "3": {
                    requiredBy: ["1"]
                },
                "4": {
                    requiredBy: ["2", "3"]
                }
            }));
        });

        it("little more complex can be sorted can be sorted", function() {
            expect(["3", "5", "4", "1", "0", "2"]).to.deep.equal(algorithms.topoSort({
                "0": {
                    requiredBy: ["1", "3", "5"]
                },
                "1": {
                    requiredBy: ["3", "4"]
                },
                "2": {
                    requiredBy: ["0", "1", "4"]
                },
                "3": {
                    requiredBy: []
                },
                "4": {
                    requiredBy: ["3", "5"]
                },
                "5": {
                    requiredBy: []
                }
            }));
        });
    });
});