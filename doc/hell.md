# Hell {#hell}

Hell is simple, decentralized, project dependency manager with [semver](http://semver.org/), [cmake](https://cmake.org/) and [git](https://git-scm.com/) integration.

In opposition to other package or dependency managers designed to work on ready to use binaries or scripts
hell was designed to be tool for compiled languages like C or C++ (but can be used for script languages also).

In case of compiled languages it's very important to have correctly compiled libraries and binaries,
which fits best our requirements - for example it matters if package was builded with Release or Debug target
or with some optional features or without them.
Sometimes important is compiler or ABI version and incorrect one may lead us to very hard to find issues.
Things are even more complicated if our project have big dependency tree where some branches may have
conflicting requirements with other.
For more in many cases "System wide" packet installation is not good solution because if we are working on different
projects at one time these projects may conflict on common dependency version or enabled in it features.

From other side things like git submodules many times not help in case when single module is required by many submodules of our project, because of similar reasons. Things can be even more complicated if our project require cross-compilation.

So we need tool which allow us to:
 * resolve our project dependency tree and inform in case of conflicts - it need to care about
   * dependency version conflicts
   * dependency required features conflicts
 * download sources of dependencies when resolved
 * build and install dependencies in correct order

**Hell can do all of these tasks for you!**

Additionally hell try to do so in very convenient and easy way:
 * Hell **use semantic versioning** - using semver range expressions you can describe acceptable for your project dependencies versions. This is small thing but give huge advantage over git submodules.
 * Hell **use heavily git**:
   * use git tags to detect software versions available to use
   * use project repository to store all data required to resolve build and install dependencies **there is no centralized database**
 * Hell **use cmake to build and install** software - today cmake is some kind of standard and every well managed project should support it.
 * Hell is written in nodejs is multi-platform and **works exactly the same on linux, mac os x, windows** and many other operating systems.
 * **Hell is pretty fast** - resolving dependency tree of 100 nodes with 100 versions and 100 configuration parameters each is done in real time even on raspberry pi.
 * Hell configuration file is **easy to read and understand** - no special magic.
 * Hell have great, pretty and colorful, constantly improved command line interface.

Because of these unique features hell will save you a lot of time, nerves and improve daily work organization.

## How to install hell?

It's simple. You just need nodejs and npm. You can install it this way:
```
npm install -g rili-hell
```
However to use it you need to have installed `git`, `cmake` and any `supported by cmake build system generator`.

## Can I use private git repositories as my project dependencies?

Yes, no problem. You only need to have access rights to these repositories.

## Does Hell support http, git, ssh, local file system repositories?

Yes, it support as much protocols as your git client support.
Hell don't have it own git implementation - it only run git binary with some arguments incantation and analyze results.

## Which cmake versions are supported?

Hell don't have itself constrains about cmake version. Only your project dependencies are important in this matter,
however cmake have very good backward-compatibility, so probably you don't need to care about it.

## I want to use hell to install my project dependencies - how to do so?

1. First of all check if project which you want use as dependency support hell.
If it support you can go directly to point 6.
2. Check if project which you want to use have proxy repo in [hell-gates](https://gitlab.com/hell-gates)
In this gitlab group you can find special kind of repos created for projects which not support hell or even
cmake or are not hosted on git by maintainers. These repos provide cmake files and hell config files which allow to use them by hell.
For example you can find there gtest, boost, clang or llvm. If you found interesting proxy repo in hell-gates you can go directly to point 6.
3. Ask maintainer of project you are interested in if he or she could add support of hell in project. If this is possible and you have enough time for that you can go to point 1.
4. Create Issue on hell project with request to add project you want to use to hell-gates - there is high possibility it can be done fast. If you can wait and your request is accepted you can go to point 2.
5. Ok. So you are here - it's time to to show you how to create your own proxy repo like these in hell-gates - go to section **I want to allow hell users to work with my project - how to do so?**
6. So you know that there is existing source repo which can be used with hell, there are git tags which points interesting for you versions - you are prepared to create your first hell configuration file.

Probably best way to introduce you into this task will be by simple example:
```
{
  "=":[
    {
      "source":"https://gitlab.com/group1/project1.git",
      "version":">1.2.3 <3.4.5",
      "required":["="]
    }
  ],
  "WITH_FEATURE_X=ON":[
    {
      "source":"https://gitlab.com/group1/project2.git",
      "version":"1.0.3",
      "required":["=", "SOMETHING=ON", "SOMETHING_ELSE=OFF"]
    }
  ],
  "WITH_FEATURE_X=OFF":[],
  "WITH_FEATURE_Y=ON":[
    {
      "source":"https://gitlab.com/group2/project1.git",
      "version":"5.x",
      "required":["=", "MAGIC_TYPE=DARK"]
    }
  ],
  "WITH_FEATURE_Y=OFF":[],
}
```

This is example of `.hell.json` file. You need to create similar for your project - it should be placed in root directory of your project repository.
Keys of root dict in this json config file(`=`, `WITH_FEATURE_X=ON`, `WITH_FEATURE_X=OFF`, `WITH_FEATURE_Y=ON` and `WITH_FEATURE_Y=OFF`) are known in hell as `targets`.
Some of targets are exclusive each other for example you cannot use both "WITH_FEATURE_Y=ON" and "WITH_FEATURE_Y=OFF" - target have 2 parts - name and value
(in case of `WITH_FEATURE_Y=ON` name is `WITH_FEATURE_Y` and value is `ON`). All hell targets contain at least on `=` character. Target name must be valid cmake variable identifier and value any kind of string.
Target `=` is special and is used as default one. Targets are very close related to cmake - hell will pass them into projects as they are so cmake will be able to detect options you want.

In case of our example config, we assume that, example project cmake support -D options: `-DWITH_FEATURE_X=ON`, `-DWITH_FEATURE_X=OFF`, `-DWITH_FEATURE_Y=ON`, `-DWITH_FEATURE_Y=OFF`.
You should expose all user configurable options in your hell config as targets.

Sometimes when you enable some kind of feature in your project, you need additional dependencies. You can describe them as target array elements. In example config if someone will select "WITH_FEATURE_Y=ON" target, then we have dependency:
```
{
  "source":"https://gitlab.com/group2/project1.git",
  "version":"5.x",
  "required":["=", "MAGIC_TYPE=DARK"]
}
```

In this notation `source` entry is git repository url, where dependency is stored. `version` field describe which semver range of versions are acceptable for us (in example case hell will accept everything with major version = 5). `required` field is array which contain targets which we want from our dependency(so in this case default(`=`) + `MAGIC_TYPE=DARK`).
Based on required field from our dependency hell can recursively go deeper and deeper in our dependency tree to find solution for us - of course in meantime it need to care about versions and targets conflicts and try resolve dependencies in another way when current is incorrect.

When you finally have prepared `.hell.json` for your projects you can try if it is valid using:
```
hell --verbose
```
This command will show you what hell found in your project or show error messages in case when your config file is not valid.
If it's valid you can try install your project dependencies this way:
```
hell --install -r <some_target> -r <some_other_target> -i <some_cmake_build_option> -i <some_other_cmake_build_option>
```
This command will resolve your dependencies, build and install them into `.hell` directory.

## I want to allow hell users to work with my project - how to do so?

@note: Check if you understand `.hell.json` file structure - it's not hard but very important.

If you know hell config structure, you know how hell resolve project dependencies and how to create your own.

Additional hints regarding your `.hell.json`:
 * remember to expose your project cmake 'user options' in `.hell.json` - otherwise your project users will not be able to manipulate them.
 * define only dependencies which are really needed for given targets - it will save time, bandwidth and computing power of your project users, required to download, build and install it.
 * try to carefully choose dependency version range - to wide may cause "compatibility break" after install, too small may cause conflicts for your project users.
 * do not try to foretell the future - always choose maximal supported version as that what really exist (exception for this is situation when you trust that your project dependency maintainer strictly follow semver rules). Otherwise there can be conflict or compatibility break in future.
 * instead of foretell you can **carefully** recreate tag with wider supported dependency version if it will be needed in future.

Cmake config files requirements which you need fulfill to allow hell to use your project as dependency:
 * You need to support cmake "install tree". If you don't know it look at this:
   * [cmake documentation: install](https://cmake.org/cmake/help/v3.0/command/install.html)
   * [cmake documentation: configure_file](https://cmake.org/cmake/help/v3.0/command/configure_file.html)
   * [cmake documentation: find_package](https://cmake.org/cmake/help/v3.0/command/find_package.html)
   * [CMake/Tutorials/How to create a ProjectConfig.cmake file](https://cmake.org/Wiki/CMake/Tutorials/How_to_create_a_ProjectConfig.cmake_file)
   * [The Ultimate Guide to Modern CMake](https://rix0r.nl/blog/2015/08/13/cmake-guide/)
   * examples:
     * [rili core](https://gitlab.com/rili/core/blob/master/CMakeLists.txt)
     * [rili testFramework](https://gitlab.com/rili/testFramework/blob/master/CMakeLists.txt)
     * [rili compatibility](https://gitlab.com/rili/compatibility/blob/master/CMakeLists.txt)
     * [rili service/time](https://gitlab.com/rili/service/time/blob/master/CMakeLists.txt)
     * [rili service/compute](https://gitlab.com/rili/service/compute/blob/master/CMakeLists.txt)
     * [rili service/calltrace](https://gitlab.com/rili/service/calltrace/blob/master/CMakeLists.txt)
     * [hell-gates boost](https://gitlab.com/hell-gates/boost/blob/master/CMakeLists.txt)
     * [hell-getes websockstpp](https://gitlab.com/hell-gates/websocketpp/blob/master/CMakeLists.txt)
     * [hell-getes googletest](https://gitlab.com/hell-gates/googletest/blob/master/CMakeLists.txt)
     * [hell-gates llvm](https://gitlab.com/hell-gates/llvm/blob/master/CMakeLists.txt)
 * You should use `find_package` to find your dependencies
 * You should generate `<YourProject>Config.cmake` file in install tree, to allow your users find your project using find_package. It should use include generated by cmake `<YourProject>Targets.cmake` file to expose installed targets additionally you should set `<YourProject>_INCLUDE_DIRS` which should contain all include search paths required to include header files of your project. Optionally you can expose other variables which might be helpful for your users. Just remember that things strictly related to cmake install targets are already well exported in `<YourProject>Targets.cmake`
 * Remember about find_package in `<YourProject>Config.cmake` for all packages your dependencies - this way your project user don't need to care about them.
 * Do not hardcode compilation flags inside your CMakeLists.txt. For most of them are cmake options which you don't want override like `CMAKE_CXX_STANDARD`, `CMAKE_BUILD_TYPE` etc.

Cmake install tree hints:
 * Try support as many platforms and compilers as you can
 * In case of proxy repositories prefer use cmake build-in features like `ExternalProject_add` against platform specific like `wget`
 * configure install tree to expose only project public interfaces
 * prefer static libraries over shared - shared/dynamic libraries introduce a lot of problems with them on different platforms because of symbol visibility
 * install stuff in standard directories:
   * `<YourProject>Config.cmake` and `<YourProject>Targets.cmake` in `${CMAKE_INSTALL_PREFIX}/lib/cmake/<YourProject>/` directory
   * libraries in: `${CMAKE_INSTALL_PREFIX}/lib/`
   * executables in: `${CMAKE_INSTALL_PREFIX}/bin/`
   * header files in: `${CMAKE_INSTALL_PREFIX}/include/<YourProject>/`

When you have prepared both `.hell.json` and install tree in cmake, you can commit changes into git repository and create first git tag. Remember that hell will care only about tags which are valid semantic versions - other are not interesting for hell. You can have without any problems tags in other form or semver tags but without `.hell.json` - hell will just filter them out.

## What about hell command line options?

All of them with description you can find using:
```
hell --help
```

most important are:
 * `--install` - if enabled then will build and install targets with cmake
 * `--required|-r` - should be used to define required targets
 * `--inject|-i` - allow to provide flags which should be used with cmake but are not deduced from targets.

## What for hell create `.hell` directory inside my project root directory?

`.hell` directory is some kind of internal hell workspace. In most cases it shouldn't be interesting for you.
You can find inside:
 * source tree in  directory `source`
 * build tree in directory `build`
 * install tree in directory `install`
 * `db.json` file which store information metadata about known for hell repositories from previous hell executions.
 * `incantations.js` file which store previous hell invocations args in current project. This is related to `--incantations` flag.

You can also define your own, custom workspace directory name using `--workspace` option.

@note: If you care about disc space and not plan to reinstall anything, then you can safely remove everything inside except `install` directory.

Inside `source` and `build` directories there are subdirs which names are hashed based on corresponding repository names.

@note: Hell will also create in your user home directory additional file: `<user_home_dir>/.hell/.incantations.json` which is exactly the same thing like project level `incantations.js` but store incantations from all your projects.

## I want to contribute. Where is hell repository?

It's here: [Hell repository](https://gitlab.com/rili/hell). You are very welcome.
